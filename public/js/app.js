const express = require('express'); 
const app = express();              //Instantiate an express app, the main work horse of this server
const port = 3000;                  //Save the port number where your server will be listening

app.use(express.static('public'));

app.get('/', (req, res) => {
    fs.readFile('public/main.html', 'utf8', (err, data) => {
        if (err) {
            res.status(404).send('Error: File not found');
        } else {
            res.type('text/html').send(data);
        }
    });
});

app.listen(port, () => {            //server starts listening for any attempts from a client to connect at port: {port}
    console.log(`Now listening on port ${port}`); 
});
